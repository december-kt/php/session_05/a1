<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="widtd=device-width initial-scale=1">

		<title>Client-Server Communication</title>
	</head>
	<body>

		<?php session_start(); ?>

		<h3>Simple Login Page</h3>

			<div>
				<form method="POST" action="./server.php" style="display: inline-block;">

					<input type="hidden" name="action" value="userlogin"/>

					<label for="useremail">Email: </label>
					<input type="email" name="useremail" required/>

					<label for="pword">Password: </label>
					<input type="Password" name="pword" required/>

					<input type="submit" value="Login">
				</form>
			</div>

	</body>
</html>